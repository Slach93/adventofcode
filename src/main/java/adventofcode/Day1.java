package adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {
    public static void main(String[] args) throws IOException {
//        (mass / 3 -> wynik zaokrąglić) - 2 ---> paliwo
//        dla każdego wpisu z pliku liczymy paliwo
//        następnie sumujemy wszystko

        Stream<String> inputValues =
        Files.lines(
                Path.of("src/main/resources/adventofcode/day1input"));
        List<String> listOfModulMass = inputValues.collect(Collectors.toList());
        int sum = 0;
        for (String mass: listOfModulMass) {
            int requiredFuel =  calculateFuel(Integer.parseInt(mass));
            sum += requiredFuel;
            System.out.println("Fuel needed for mass " +  "-->>" + requiredFuel);

        }

        System.out.println("Sum of required fuel " + sum);
    }
    public static int calculateFuel(int mass){
         return (mass / 3) -2;

    }
}
