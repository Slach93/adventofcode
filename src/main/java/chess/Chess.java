package chess;

import java.util.Scanner;

public class Chess {

    public static final String WHITE_FIELD = " \u25A0 ";
    public static final String BLACK_FIELD = " \u25A1 ";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Podaj długość szachownicy");
        int chess_lenght = scanner.nextInt();
        System.out.println("Rysujesz szachownice o długości " + chess_lenght);

        for (int y = 0; y < chess_lenght; y++) {
        for (int x = 0; x < chess_lenght; x++) {
            if ((x + y) % 2 == 0) {
                System.out.print(BLACK_FIELD);
            } else {
                System.out.print(WHITE_FIELD);
            }
            }
            System.out.println("");
        }
    }
}


