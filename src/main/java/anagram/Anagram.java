package anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {
        System.out.println("Podaj dwa słowa, a sprawdzę czy są anagramami");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwsze słowo: ");
        String firstWord = scanner.nextLine();
        System.out.println("Podaj drugie słowo: ");
        String secondWord = scanner.nextLine();

        char[] firstWordCharacters = firstWord.toCharArray();
        char[] secondWordCharacters = firstWord.toCharArray();

        Arrays.sort(firstWordCharacters);
        Arrays.sort(secondWordCharacters);
        boolean areEqual = Arrays.equals(firstWordCharacters, secondWordCharacters);

        if (areEqual) {
            System.out.println("Podane słowa sa anagramami ");
        } else {
            System.out.println("Podane słowa nie są anagramami ");
        }
    }
}
